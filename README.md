# Kanap #

Issue du programme OpenClassRooms

### Pré-requis pour le Back-end ###

- Node
- NPM

### Installation du Back-end ###

- Depuis le dossier back, lancer `npm install` pour installer les dépendences du projet
- Lancer le server via la commande `node server`
- Le serveur écoute le port 3000 par défaut. Si il est déja pris, écoute un autre port qui est préciser au démarrage
