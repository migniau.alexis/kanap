import Cart from "./Model/Cart.js";
import CartProduct from "./Model/CartProduct.js";
import api from "./Service/api.js"

// Affiche les données d'un produit
async function printProduct()
{
    // Récupération du l'id depuis l'url
    const url = new URL(window.location.href);
    const id = url.searchParams.get('id');

    const product = await api.getProduct(id);

    // Balise image
    document.querySelector('article > .item__img').innerHTML = product.getImageTag();
    // Nom du produit
    document.getElementById('title').innerHTML = product.name;
    // Prix
    document.getElementById('price').innerHTML = product.price;
    // Description
    document.getElementById('description').innerHTML = product.description;
    // Couleurs
    let colorsSelect = document.getElementById('colors')
    colorsSelect.insertAdjacentHTML('beforeend', product.getOptionColors())

    // Réagit au clique sur le bouton d'ajout au panier
    document.getElementById('addToCart').addEventListener('click', function () {
        let cart = new Cart();

        // Récupération des valeurs depuis l'HTML et ajout dans le panier
        cart.addProduct(new CartProduct({
            'id' : id,
            'color' : colorsSelect.value,
            'quantity' : parseInt(document.getElementById('quantity').value)
        }))
    })
}

printProduct()