// Réprésente un produit du panier
class CartProduct {
    constructor(product)
    {
        this.id = product.id;
        this.quantity = product.quantity;
        this.color = product.color;
    }
}

export default CartProduct