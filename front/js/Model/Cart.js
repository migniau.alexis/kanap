import api from "../Service/api.js";
import CartProduct from "./CartProduct.js"

class Cart {
    constructor()
    {
        // Charge depuis le localStorage
        const cart = JSON.parse(localStorage.getItem('cart')) ?? [];

        // Transforme les objet en CartProduct
        this.products = cart.map(product => new CartProduct(product));
    }

    // Ajout un produit dans le panier si il n'est pas déja présent sinon ajoute la quantité supplémentaire
    addProduct(productToAdd)
    {
        // Utilisation d'un boolean pour détecter si c'est un nouveau produit
        let newProduct = true;
        this.products = this.products.map((product) => {
            // Si on trouve un produit avec le même id et même couleur dans le panier
            if(product.id == productToAdd.id && product.color == productToAdd.color)
            {
                newProduct = false;
                product.quantity += productToAdd.quantity
            }

            return product;
        })

        // Si c'est bien un nouveau produit on l'ajoute au tableau
        if(newProduct)
            this.products.push(productToAdd);

        this.saveToLocalStorage()
    }

    // Modifie la quantité d'un produit du panier selon son id et sa couleur
    updateQuantity(id, color, newQuantity) {
        this.products = this.products.map(product => {
            if(product._id == id && product.color == color)
            {
                product.quantity = newQuantity
            }

            return product
        })
        this.saveToLocalStorage()
    }

    // Supprime un produit du panier selon son id et sa couleur
    deleteProduct(id, color) {
        this.products = this.products.filter(product => !(product._id == id && product.color == color))
        this.saveToLocalStorage()
    }

    // Sauvegarde dans le localStorage
    saveToLocalStorage()
    {
        localStorage.setItem('cart', JSON.stringify(this.products))
    }

    // Retourne la quantité total de produit
    getTotalQuantity() {
        return this.products.reduce((quantity, product) => quantity + product.quantity, 0)
    }

    // Retourne le prix total du panier
    async getTotalPrice() {
        return await this.products.reduce(async (price, productFromCart) => {
            const productFromApi = await api.getProduct(productFromCart.id)
            return await price + productFromApi.price * productFromCart.quantity
        }, 0)
    }
} 

export default Cart