// Produit de l'api
class Product {
    // Construction depuis un JSON
    constructor(json)
    {
        this.id = json._id;
        this.name = json.name;
        this.price = json.price;
        this.imageUrl = json.imageUrl;
        this.description = json.description;
        this.altTxt = json.altTxt;
        // Array de string avec les couleurs disponible
        this.colors = json.colors;
    }

    // Retourne l'html qui est utiliser pour la page d'accueil
    toHtmlForHome() {
        return `
            <a href="./product.html?id=${this.id}">
                <article>
                    ${this.getImageTag()}
                    <h3 class="productName">${this.name}</h3>
                    <p class="productDescription">${this.description}</p>
                </article>
            </a>
        `
    }

    // Retourne la balise img
    getImageTag() {
        return `<img src="${this.imageUrl}" alt="${this.altTxt}">`
    }

    // Retourne les options HTML pour les couleurs
    getOptionColors() {
        return this.colors.reduce((html, color) => html += `<option value="${color}">${color}</option>`, '')
    }

    // Retourne l'HTML pour la page du panier, nécessite les données provenant du localStorage
    toHTMLForCart(quantity, color) {
        return `
            <article class="cart__item" data-id="${this.id}" data-color="${color}">
                <div class="cart__item__img">
                    ${this.getImageTag()}
                </div>
                <div class="cart__item__content">
                    <div class="cart__item__content__description">
                        <h2>${this.name}</h2>
                        <p>${color}</p>
                        <p>${this.price * quantity} €</p>
                    </div>
                    <div class="cart__item__content__settings">
                        <div class="cart__item__content__settings__quantity">
                            <p>Qté : </p>
                            <input type="number" class="itemQuantity" name="itemQuantity" min="1" max="100" value="${quantity}">
                        </div>
                        <div class="cart__item__content__settings__delete">
                            <p class="deleteItem">Supprimer</p>
                        </div>
                    </div>
                </div>
            </article>
        `
    }
}

export default Product