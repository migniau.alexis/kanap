// Réprésente l'objet Contact qui sert lors d'une commande
class Contact {
    constructor(firstName, lastName, address, city, email)
    {
        this.firstName = firstName
        this.lastName = lastName
        this.address = address
        this.city = city
        this.email = email
    }

    // Valide tous les champs et retourne les erreurs à affichés sous forme de tableau : [NOM_CHAMP : MESSAGE_ERREUR, 'firstName' => 'Prénom invalide', ...]
    validate()
    {
        let error = [];

        // Regex pour vérifier qu'un text ne contient pas de nombre
        const containsDigit = new RegExp(/\d/)

        if(containsDigit.test(this.firstName))
        {
            error['firstName'] = 'Votre prénom ne peut pas contenir de chiffre'
        }

        if(containsDigit.test(this.lastName))
        {
            error['lastName'] = 'Votre nom ne peut pas contenir de chiffre'
        }

        if(containsDigit.test(this.city))
        {
            error['city'] = 'Votre ville ne peut pas contenir de chiffre'
        }

        if(!(new RegExp(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/).test(this.email)))
        {
            error['email'] = 'Votre email ne respecte pas le format standard'
        }

        return error;
    }
}

export default Contact