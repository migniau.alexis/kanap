import Cart from "./Model/Cart.js"
import Contact from "./Model/Contact.js";
import api from "./Service/api.js"

// Récupération du panier
let cart = new Cart()

async function createHTML()
{
    let sectionCartItems = document.getElementById('cart__items');
    
    let promiseHTML = new Promise((resolve, reject) => {
        cart.products.forEach(async (productFromCart, index, array) => {
            // Récupération des données du produit
            const productFromApi = await api.getProduct(productFromCart.id)
    
            // Création du bloc HTML
            sectionCartItems.insertAdjacentHTML('beforeend' ,productFromApi.toHTMLForCart(productFromCart.quantity, productFromCart.color))
            // Retour du promise si dernière itération
            if(index === array.length - 1)
                resolve();
        });
    });

    promiseHTML.then(() => {
        // Ajout de listener pour tous les boutons supprimer
        document.querySelectorAll('.deleteItem').forEach((element) => element.addEventListener('click', onDeleteButton))

        // Ajout de listener pour le changement de quantité
        document.querySelectorAll('.itemQuantity').forEach((element) => element.addEventListener('change', onQuantityChange))
    })

    // Création d'un listener sur le bouton 'Commander'
    document.getElementById('order').addEventListener('click', onOrder)

    updateTotal()
}

createHTML()

// Réagit au clique du bouton supprimer
function onDeleteButton(event) {
    // Récupére la balise article parent pour connaitre l'id et la color du produit à supprimer
    let article = event.target.closest('article')
    cart.deleteProduct(article.dataset.id, article.dataset.color)

    // Suppression de l'HTML
    article.remove()

    updateTotal()
}

// Réagit au changement de quantité
async function onQuantityChange(event) {
    // Récupére la balise article parent pour connaitre l'id et la color du produit à supprimer
    let article = event.target.closest('article')
    cart.updateQuantity(article.dataset.id, article.dataset.color, parseInt(event.target.value))

    // Récupération du produit depuis l'api pour recalculer le prix
    const product = await api.getProduct(article.dataset.id)

    // Mise à jour du prix total pour le produit
    document.querySelector(`article[data-id="${article.dataset.id}"][data-color="${article.dataset.color}"] > .cart__item__content > .cart__item__content__description > :nth-child(3)`).innerHTML = product.price * parseInt(event.target.value) + ' €'

    updateTotal()
}

// Mise à jour des totaux
async function updateTotal() {
    document.getElementById('totalQuantity').innerHTML = cart.getTotalQuantity()
    document.getElementById('totalPrice').innerHTML = await cart.getTotalPrice()
}

// Commande
async function onOrder(event) {
    // On annule l'envoie du formulaire
    event.preventDefault()

    // Création de l'objet Contact depuis les donnée du formulaire
    let contact = new Contact(
        document.getElementById('firstName').value,
        document.getElementById('lastName').value,
        document.getElementById('address').value,
        document.getElementById('city').value,
        document.getElementById('email').value
    )

    // Validation du formulaire
    let errors = await contact.validate();
    
    // Affichage des erreurs
    for(let field in errors)
    {
        document.getElementById(field + 'ErrorMsg').innerHTML = errors[field];
    }

    // Si aucune erreur alors redirection sur la page de confirmation
    if(errors.length == 0)
    {
        const orderId = await api.sendOrder(contact, cart.products.map(p => p.id))

        // Renvoie sur la page de confirmation
        window.location.href = "confirmation.html?orderId=" + orderId;
    }
}