import Product from "../Model/Product.js"

// Retourne tous les produits
const getProducts = async () => 
{
    const res = await fetch('http://localhost:3000/api/products');
    const json = await res.json();

    // Transforme le JSON en objet produit
    return json.map(product => new Product(product))
}

// Retourne un produit selon son ID
const getProduct = async (id) => {
    const res = await fetch(`http://localhost:3000/api/products/${id}`);
    const json = await res.json();

    return new Product(json);
}

// Envoie une commande et retourne l'id de la commande
const sendOrder = async (contact, products) => {
    const res = await fetch(`http://localhost:3000/api/products/order`, {
        method : "POST",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body : JSON.stringify({contact, products})
    });
    const json = await res.json();

    return json.orderId
}

export default {getProducts, getProduct, sendOrder}