import api from "./Service/api.js"

// Affiche tous les produits
async function printProducts()
{
    // Fetch des produits via l'API
    const products = await api.getProducts()
    let itemsDiv = document.getElementById('items')

    products.forEach(product => {
        itemsDiv.innerHTML += product.toHtmlForHome()
    });
}

printProducts()